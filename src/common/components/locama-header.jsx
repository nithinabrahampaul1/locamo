import React, { useEffect, useState } from "react";
import {
  Nav,
  Navbar,
  Container,
  Button,
} from "react-bootstrap";

export const LocamoHeader = () => {
  const [isHidden, setIsHidden] = useState(false);

  const hideBar = () => {
    const pos = window.pageYOffset;
    pos > 120 ? setIsHidden(true) : setIsHidden(false)
  }

  useEffect(() => {
    window.addEventListener("scroll", hideBar, { passive: true });

    return () => {
      window.removeEventListener("scroll", hideBar);
    };
  }, []);

  let classHide = isHidden ? "hide" : ""

  return (
    <Navbar bg="none" expand="lg" className={`topnav ${classHide}`}>
      <Container>
        <Navbar.Brand href="/">
          <img style={{
            width: 70,
            height: 65
          }} src="assets/images/Group 1.png" alt="logo" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" className="customNavbar"/>
        <Navbar.Collapse id="basic-navbar-nav"  style={{ justifyContent: 'flex-end' }}>
          <Nav style={{ paddingRight: '.8rem' }}>
            <Nav.Link className="nav-link" href="#home">Home</Nav.Link>
            <Nav.Link className="nav-link" href="#about">About</Nav.Link>
            <Nav.Link className="nav-link" href="#business">Business</Nav.Link>
            <Nav.Link className="nav-link" href="#team">Team</Nav.Link>
            <Nav.Link className="nav-link" href="#contact">Contact</Nav.Link>
          </Nav>
          <Button className="gray-btn">shop now</Button>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Form, InputGroup, Image } from "react-bootstrap";

export const LocamoFormInput = React.forwardRef(
  ({ label, placeholder, rightIcon, type = "text", leftIcon, error, ...rest }, ref) => {
    return (
      <Form.Group className="mb-4">
        <Form.Label>{label}</Form.Label>
        <InputGroup>
          {leftIcon && (
            <InputGroup.Text>
              <FontAwesomeIcon icon={leftIcon} />
            </InputGroup.Text>
          )}
          <Form.Control
            ref={ref}
            type={type}
            placeholder={placeholder}
            {...rest}
          />
          {rightIcon && (
            <InputGroup.Text>
              <Image src={rightIcon} />
            </InputGroup.Text>
          )}
        </InputGroup>
        {error && <span className="form_error">{error?.message}</span>}
      </Form.Group>
    );
  }
);

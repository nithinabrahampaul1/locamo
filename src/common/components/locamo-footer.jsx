import React from "react";
import {
  Col,
  Container,
  Image,
  Row,
} from "react-bootstrap";
import { LocamoFormInput } from "./locamo-forminput";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitterSquare, faFacebook, faLinkedin, faDiscord } from "@fortawesome/free-brands-svg-icons";

export const LoacmoFooter = () => {
  return (
    <div style={{ backgroundColor: "#163d47" }}>
      <Container className="footer-content">
        <Row style={{ display: "flex", alignItems: "center" }}>
          <Col xs={12} sm={12} md={6} lg={8} style={{ padding: 0 }}>
            <h5 style={{ color: "#ffffff", fontSize: 20, fontWeight: "bold" }}>
              Share and follow
            </h5>
            <h5 style={{ color: "#ffffff", fontSize: 13, fontWeight: "normal" }}>
              Looking cautiously round, to ascertain that they were not
              overheard
            </h5>
          </Col>
          <Col xs={12} sm={12} md={6} lg={4} style={{ padding: 0 }}>
            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>
              <FontAwesomeIcon icon={faFacebook} className="footer-icon" style={{ backgroundColor: '#3b5998', color: '#fff', borderRadius: '50%', padding: 16, margin: 10 }} />
              <FontAwesomeIcon icon={faLinkedin} className="footer-icon" style={{ backgroundColor: '#0077b5', color: '#fff', borderRadius: '50%', padding: 16, margin: 10 }} />
              <FontAwesomeIcon icon={faTwitterSquare} className="footer-icon" style={{ backgroundColor: '#1da1f2', color: '#fff', borderRadius: '50%', padding: 16, margin: 10 }} />
              <Image src={'assets/svg/insta.svg'} style={{ height: 70 }} />
              <FontAwesomeIcon icon={faDiscord} className="footer-icon" style={{ color: '#fff', borderRadius: '50%', backgroundColor: '#6665d2', padding: 16, margin: 10 }} />
            </div>
          </Col>
          <div
            style={{ borderTop: "1px solid #6975a7", margin: "50px 0" }}
          ></div>
        </Row>
        <Row>
          <Col xs={12} sm={6} md={6} lg={4} style={{ padding: 0 }}>
            <h5
              style={{
                color: "#ffffff",
                fontSize: 20,
                fontWeight: "bold",
                padding: "0 0 20px 0",
              }}
            >
              About Locamo
            </h5>
            <h1 className='f-text' style={{ color: '#ffffff', fontSize: 13, fontWeight: 'normal' }}>Looking cautiously round, to ascertain that they were not overheard, the two hags cowered nearer to the fire, and chuckled heartily.</h1>
            <Row style={{ padding: '50px 0px' }}>
              <Col xs={12} sm={6}>
                <div style={{ color: "#ef3340", padding: '5px 0', fontSize: '13px', fontWeight: 'normal' }}>What is an ICO?  <Image style={{ width: 7 }} src={"assets/svg/caret-right-solid.svg"} /></div>
                <div style={{ color: "#ef3340", padding: '5px 0', fontSize: '13px', fontWeight: 'normal' }}>Press about us  <Image style={{ width: 7 }} src={"assets/svg/caret-right-solid.svg"} /></div>
                <div style={{ color: "#ef3340", padding: '5px 0', fontSize: '13px', fontWeight: 'normal' }}>Road Map  <Image style={{ width: 7 }} src={"assets/svg/caret-right-solid.svg"} /></div>
                <div style={{ color: "#ef3340", padding: '5px 0', fontSize: '13px', fontWeight: 'normal' }}>Architecture  <Image style={{ width: 7 }} src={"assets/svg/caret-right-solid.svg"} /></div>
              </Col>
              <Col xs={12} sm={6}>
                <div style={{ color: "#ef3340", padding: '5px 0', fontSize: '13px', fontWeight: 'normal' }}>About Locamo ICO?  <Image style={{ width: 7 }} src={"assets/svg/caret-right-solid.svg"} /></div>
                <div style={{ color: "#ef3340", padding: '5px 0', fontSize: '13px', fontWeight: 'normal' }}>Blockchain  <Image style={{ width: 7 }} src={"assets/svg/caret-right-solid.svg"} /></div>
                <div style={{ color: "#ef3340", padding: '5px 0', fontSize: '13px', fontWeight: 'normal' }}>Whitepaper  <Image style={{ width: 7 }} src={"assets/svg/caret-right-solid.svg"} /></div>
              </Col>
            </Row>
          </Col>
          <Col xs={12} sm={6} md={6} lg={4}>
            <h5
              style={{
                color: "#ffffff",
                fontSize: 20,
                fontWeight: "bold",
                padding: "0 0 20px 0",
              }}
            >
              Instagram
            </h5>
            <h1 style={{ fontSize: 15, color: "#6c7496" }}>
              <span style={{ color: "#FFF" }}>@locamo.ae</span>
            </h1>
            <Row>
              <Col xs={3} sm={3} style={{ padding: "1px 1px" }}>
                <Image className="f-img" src="assets/images/img1.jpg" />
              </Col>
              <Col xs={3} sm={3} style={{ padding: "1px 1px" }}>
                <Image className="f-img" src="assets/images/img2.jpg" />
              </Col>
              <Col xs={3} sm={3} style={{ padding: "1px 1px" }}>
                <Image className="f-img" src="assets/images/img3.jpg" />
              </Col>
              <Col xs={3} sm={3} style={{ padding: "1px 1px" }}>
                <Image className="f-img" src="assets/images/img4.jpg" />
              </Col>
              <Col xs={3} sm={3} style={{ padding: "1px 1px" }}>
                <Image className="f-img" src="assets/images/img5.jpg" />
              </Col>
              <Col xs={3} sm={3} style={{ padding: "1px 1px" }}>
                <Image className="f-img" src="assets/images/img6.jpg" />
              </Col>
              <Col xs={3} sm={3} style={{ padding: "1px 1px" }}>
                <Image className="f-img" src="assets/images/img7.jpg" />
              </Col>
              <Col xs={3} sm={3} style={{ padding: "1px 1px" }}>
                <Image className="f-img" src="assets/images/img8.jpg" />
              </Col>
            </Row>
          </Col>
          <Col xs={12} sm={12} md={6} lg={4} className='subscribe'>
            <h5 style={{ color: "#ffffff", fontSize: 20, fontWeight: "bold" }}>
              Subscribe to newsletter
            </h5>
            <LocamoFormInput placeholder="Your email" rightIcon='assets/svg/email.svg' />
            <h1 style={{ color: "#FFF", fontSize: 13, fontWeight: "normal" }}>
              Looking cautiously round, to ascertain that they were not
              overheard, the two hags cowered nearer to the fire, and chuckled
              heartily.
            </h1>
          </Col>
        </Row>
        <h1 style={{ color: '#6c7496', fontSize: 13, fontWeight: 'bold' }}>© 2022 Locamo—All rights reserved</h1>
      </Container>
    </div>
  );
};

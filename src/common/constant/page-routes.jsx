import { componentRoutes } from ".";
import Pages from "../../pages";

export const rootRoutes = [
  {
    path: componentRoutes.root,
    component: Pages,
  },
];

export const employeeRoutes = [

];

export const pageRoutes = [...rootRoutes, ...employeeRoutes];

const rootRoutes = {
  root: "/",
};

const employeeRoutes = {
};

const adminRoutes = {};

export const componentRoutes = {
  ...rootRoutes,
  ...employeeRoutes,
  ...adminRoutes,
};

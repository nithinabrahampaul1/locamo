import React, { useState } from "react";
import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Image,
  OverlayTrigger,
  Row,
  Tab,
  Tabs,
  Tooltip,
} from "react-bootstrap";
import { LocamoHeader } from "../../common/components/locama-header";
import { LocamoFormInput } from "../../common/components/locamo-forminput";
import { LoacmoFooter } from "../../common/components/locamo-footer";
import Invesment from "../../assets/svg/investment.svg";
import StoryBg from "../../assets/svg/storyBg.svg";

let data = [
  {
    name: "Aiden McBride",
    des: "VP of Business Development",
    img: "assets/images/Oval (1).png",
  },
  {
    name: "Leroy Fowler",
    des: "Chief Business Officer EMEA",
    img: "assets/images/Oval (2).png",
  },
  {
    name: "Billy Rose",
    des: "Chief Business Officer EMEA",
    img: "assets/images/Oval (3).png",
  },
  {
    name: "Jessie Adams",
    des: "Chief Legal Officer",
    img: "assets/images/Oval (4).png",
  },
  {
    name: "Jason Mathis",
    des: "Chief Operating Officer",
    img: "assets/images/Oval (5).png",
  },
  {
    name: "Martha Gilbert",
    des: "Chief Legal Officer",
    img: "assets/images/Oval (6).png",
  },
  {
    name: "Martin Lopez",
    des: "PR and Marketing Manager",
    img: "assets/images/Oval (7).png",
  },
  {
    name: "Ian Brady",
    des: "chief Executive Officer",
    img: "assets/images/Oval (8).png",
  },
  {
    name: "Billy Rose",
    des: "Chief Business Officer EMEA",
    img: "assets/images/Oval (9).png",
  },
  {
    name: "Leroy Fowler",
    des: "Chief Business Officer EMEA",
    img: "assets/images/Oval (10).png",
  },
  {
    name: "Aiden McBride",
    des: "VP of Business Development",
    img: "assets/images/Oval (11).png",
  },
  {
    name: "Jessie Adams",
    des: "Chief Legal Officer",
    img: "assets/images/Oval (12).png",
  },
  {
    name: "Martin Lopez",
    des: "PR and Marketing Manager",
    img: "assets/images/Oval (13).png",
  },
  {
    name: "Jessie Adams",
    des: "Chief Operating Officer",
    img: "assets/images/Oval (14).png",
  },
  {
    name: "Ian Brady",
    des: "chief Executive Officer",
    img: "assets/images/Oval (15).png",
  },
];
const style1 = {
  display: 'flex',
  flexWrap: 'nowrap',
  alignItems: 'stretch',
  margin: 0,
  padding: 0,
  background: '#f5f5f5',
  borderRadius: 30,
}
const style3 = {
  display: 'flex',
  flexWrap: 'nowrap',
  alignItems: 'stretch',
  margin: 0,
  padding: 0,
  background: '#4f869a',
  borderRadius: 30,
}

const style2 = {
  flex: 1,
  lineHeight: 0,
  textAlign: 'center',
}
const renderTooltip = (props) => (
  <Tooltip id="button-tooltip" {...props}>
    ICO/IDO
  </Tooltip>
);
const renderTooltip1 = (props) => (
  <Tooltip id="button-tooltip" {...props}>
    Research and Development
  </Tooltip>
);
const renderTooltip2 = (props) => (
  <Tooltip id="button-tooltip" {...props}>
    Founders N Team memeber
  </Tooltip>
);
const renderTooltip3 = (props) => (
  <Tooltip id="button-tooltip" {...props}>
    Marketing
  </Tooltip>
);
const renderTooltip4 = (props) => (
  <Tooltip id="button-tooltip" {...props}>
    Bounty
  </Tooltip>
);
const renderTooltip5 = (props) => (
  <Tooltip id="button-tooltip" {...props}>
    Leagal Compliance
  </Tooltip>
);
const renderTooltip6 = (props) => (
  <Tooltip id="button-tooltip" {...props}>
    Private Sale
  </Tooltip>
);
export const Home = () => {
  const [isHighLighted, setHighLighted] = useState(true)
  const [isInvestHighLighted, setInvestHighLighted] = useState(true)
  return (
    <div>
      <LocamoHeader />
      <Container id="home" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: 'calc(100vh + 125px)' }}>
        <Row className="home" >
          <Col
            xs={12}
            sm={12}
            md={12}
            lg={5}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "flex-start",
              flexDirection: "column",
            }}
          >
            <h1 className="header-title" >
              A Violent blend of online and offline retail worlds…
            </h1>
            <div className="mt-2">
              <Button className="red-btn" style={{ marginBottom: 2 }}>
                white paper
              </Button>
              <Button className="transpernt-btn" style={{ marginBottom: 2 }}>
                learn more
              </Button>
            </div>
            <div className="mt-5">
              <Image
                className="hade-icon"
                src="assets/images/Path 2260.png"
              />
              <Image
                className="hade-icon"
                src="assets/images/Path 2259.png"
              />
              <Image
                className="hade-icon"
                src="assets/images/Group 451.png"
              />
              <Image
                className="hade-icon"
                src="assets/images/Group 433.png"
              />
            </div>
          </Col>
          <Col xs={12} sm={12} md={12} lg={7}>
            <Image src="assets/images/Home F2.png" style={{ width: '100%' }} />
          </Col>
        </Row>
      </Container>
      <div
        id="about"
        style={{
          backgroundImage: `url(${StoryBg})`,
          backgroundPosition: "center",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          width: "100%",
        }}
      >
        <Container className="p-7 px-3">
          <Row>
            <Col
              xs={12}
              sm={12}
              md={12}
              lg={6}
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "flex-start",
                flexDirection: "column",
              }}
            >
              <h1 className="header-title" >
                LOCAMO’s STORY{" "}
              </h1>
              <p className="story">
                With Locamo, we are creating something truly sustainable that
                has a high social benefit. Our vision is to establish hybrid
                shopping as the “new normal” and thus reduce harmful CO
                emissions by eliminating long transport routes and stabilize
                regional economic cycles. To this end, we want to support tens
                of thousands of stationary retailers in their digitalization and
                unite them on one single platform that connects the online and
                offline worlds and offers so called hybrid shoppers, which
                combines online and offline shopping, true connected commerce.
                66% of all consumers want the regional offer in digital form and
                would prefer their regional retailer when shopping online.
              </p>
              <div className="mt-3 mb-2">
                <Button className="red-btn">
                  read more
                </Button>
              </div>
            </Col>
            <Col xs={12} sm={12} md={12} lg={6}>
              <Image src="assets/images/Group 397.png" style={{ width: '100%' }} />
            </Col>
          </Row>
        </Container>
      </div>
      <div id="business" style={{ backgroundColor: "#ef3340" }}>
        <Container style={{
          padding: "100px 12px 30px 12px"
        }}>
          < div className="locamo-pargraph" >
            <h1 className="text-locamo">
              <span style={{ color: "#ef3340", fontWeight: 'bold' }}>LOCAMO</span> establishes
              “hybrid shopping” as the “new normal” and thus reduces harmful CO
              emissions by eliminating long transport routes and stabilizes
              regional economic cycles.
            </h1>
          </div>
          <div className="hybrid">
            <h1 className="text-locamo-pargraph">Hybrid Shopping:</h1>
            <h1 className="text-locamo-pargraph" style={{ fontWeight: 'bold' }}>
              LOCAMO <span style={{ fontWeight: "normal" }}>supports tens of thousands of bricks and mortar retailers
                in their digitalization and unites them on one single platform
                that connects the online and offline worlds and offers consumers
                true connected commerce.</span>
            </h1>
          </div>
        </Container>
      </div >
      <Container className={'invest-text'}>
        <h1 className="invest-pargraph">
          INVESTMENT HIGHLIGHTS
        </h1>
        <Row>
          <Col xs={12} sm={12} md={12} lg={6}>
            <Row className="invest-pargraph-1" onMouseEnter={setInvestHighLighted.bind(this, false)} onMouseLeave={setInvestHighLighted.bind(this, true)}>
              <Col
                xs={12}
                sm={3}
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image style={{ width: 95 }} src={"assets/images/Group 465.png"} />
              </Col>
              <Col xs={12} sm={9}>
                <h1 className="black-text">
                  Unique team composition
                </h1>
                <h1 className="black-20-text">
                  The executive team{" "}
                  <span className="red-text">
                    combines complementary expertise
                  </span>
                  from renowned{" "}
                  <span className="red-text">
                    e-commerce experts
                  </span>{" "}
                  and reputable advisors.
                </h1>
              </Col>
            </Row>
            <Row className={(isInvestHighLighted) ? "invest-pargraph-2" : "invest-pargraph-1"} onMouseEnter={setInvestHighLighted.bind(this, true)} onMouseLeave={setInvestHighLighted.bind(this, false)}>
              <Col
                xs={12}
                sm={3}
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image style={{ width: 95 }} src="assets/images/Group 453.png"></Image>
              </Col>
              <Col xs={12} sm={9}>
                <h1 className="black-text" style={{ color: isInvestHighLighted ? '#FFF' : '' }}>
                  Immense market potential
                </h1>
                <h1 className="black-20-text" style={{ color: isInvestHighLighted ? '#FFF' : '' }}>
                  E-commerce is an
                  <span className="red-text" style={{ marginRight: 4 }}>
                    enormous and rapidly growing market, with a 45% growth
                  </span>
                  predicted in middle east and india over the next 4 years,
                  further pushed by Covid-19.
                </h1>
              </Col>
            </Row>
            <Row className="invest-pargraph-1" onMouseEnter={setInvestHighLighted.bind(this, false)} onMouseLeave={setInvestHighLighted.bind(this, true)}>
              <Col
                xs={12}
                sm={3}
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image style={{ width: 95 }} src="assets/images/Group 462.png"></Image>
              </Col>
              <Col xs={12} sm={9}>
                <h1 className="black-text">
                  Proven traction as a cornerstone for strong growth
                </h1>
                <h1 className="black-20-text">
                  <span className="red-text-token">
                    LOCAMO
                  </span>{" "}
                  has
                  <span className="red-text">
                    continuously optimized its platform
                  </span>
                  over the past 4 years and now has{" "}
                  <span className="red-text">
                    a strong focus on profitable growth.
                  </span>
                </h1>
              </Col>
            </Row>
          </Col>
          <Col
            xs={12}
            sm={12}
            md={12}
            lg={6}
            className="d-flex"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image src={Invesment} style={{ paddingLeft: "20px", width: "100%" }} className='invset-img' />
          </Col>
        </Row>
      </Container>
      <div className="rd-sec">
        <Container
          className="bg-img">
          <Row
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Col
              xs={12}
              sm={12}
              md={6}
              lg={4}
              className="locamo-text"
            >
              <h6 className="locamo-text-title" style={{ fontSize: 18 }}>
                LOCAMO Total Supply
              </h6>
              <h3 className="locamo-text-pargraph" style={{ fontSize: 30 }}>
                5000000 LLL
              </h3>
            </Col>
            <Col
              xs={12}
              sm={12}
              md={6}
              lg={4}
              className="total"
            >
              <h6 className="locamo-text-pargraph" style={{ fontSize: 18, lineHeight: 1.5 }}>
                Token Standard
              </h6>
              <h3 className="locamo-text-title" style={{ fontSize: 30 }}>
                Polygon Initially
              </h3>
            </Col>
            <Col
              xs={12}
              sm={12}
              md={12}
              lg={4}
              className="token"
            >
              <Button className="red-btn">
                BUY TOKEN
              </Button>
            </Col>
          </Row>
        </Container>
      </div>
      <Container style={{ padding: "100px 0" }}>
        <h1 className="header-title" style={{ textAlign: 'center', marginBottom: 50, marginTop: 50 }}>
          Token Sale Stages
        </h1>
        <Row>
          <Col
            xs={12}
            md={12}
            lg={4}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
            }}
            className="token-sale-stage"
          >
            <Card className="card-width" onMouseEnter={setHighLighted.bind(this, false)} onMouseLeave={setHighLighted.bind(this, true)}>
              <Card.Body>
                <Card.Title className="cart-title">
                  Community Pre-sale
                </Card.Title>
                <div style={{ borderTop: "1px solid #e6e6e6" }}></div>
                <Card.Text
                  style={{
                    marginTop: 10,
                    textAlign: "center",
                    fontWeight: "bold",
                  }}
                >
                  <h1 className="card-title-text">
                    Target
                  </h1>
                  <h1 className="card-count">
                    $ 300 000
                  </h1>
                  <h1 className="card-count-text">
                    3 000 000 Locamo
                  </h1>
                  <div
                    style={{
                      padding: "45px  0",
                      display: "flex",
                      alignItems: "baseline",
                      justifyContent: 'center'
                    }}
                  >
                    <h1 className="card-price" >
                      Locamo Price
                    </h1>
                    <Button className="card-btn">
                      $ 0.01
                    </Button>
                  </div>
                </Card.Text>
                <h1 className="card-fixed">
                  Fixed personal allocation
                </h1>
                <div className="tab-token">
                  <Tabs
                    style={{
                      backgroundColor: "rgba(65,73,107,0.10196078431372549 )",
                      borderRadius: 30,
                      margin: 0
                    }}
                    fill justify
                    style={style1}
                    defaultActiveKey="home"
                    id="uncontrolled-tab-example"
                    className="mb-3 uncontrolled"
                  >
                    <Tab
                      eventKey="home"
                      title="$500"
                      style={{ textAlign: "center" }}
                      style={style2}
                    >
                      <h1
                        style={{
                          color: "#ef3340",
                          fontSize: 17,
                          fontWeight: "bold",
                          marginTop: 30,
                        }}
                      >
                        Lockup
                      </h1>
                      <h1 className="tab-text">
                        12-month lockup followed by a 12-month linear release
                      </h1>
                    </Tab>
                    <Tab
                      eventKey="profile"
                      disabled={true}
                      title="5000 LLL"
                      style={{ textAlign: "center" }}
                      style={style2}
                    >
                      <h1
                        style={{
                          color: "#ef3340",
                          fontSize: 17,
                          fontWeight: "bold",
                          marginTop: 50,
                        }}
                      >
                        Lockup
                      </h1>
                      <h1 className="tab-text">
                        12-month lockup followed by a 12-month linear release
                      </h1>
                    </Tab>
                  </Tabs>
                </div>
              </Card.Body>
            </Card>
          </Col>
          <Col
            xs={12}
            md={12}
            lg={4}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
            }}
            className="token-sale-stage"
          >
            <Card className={(isHighLighted) ? "card-dark-width" : "card-width"} onMouseEnter={setHighLighted.bind(this, true)} onMouseLeave={setHighLighted.bind(this, false)}>
              <Card.Body>
                <Card.Title className={(isHighLighted) ? "cart-title-dark" : "cart-title"}>
                  Community Sale
                </Card.Title>
                <div style={{ borderTop: "1px solid #e6e6e6" }}></div>
                <Card.Text style={{ marginTop: 10, textAlign: "center" }}>
                  <h1 className={(isHighLighted) ? "card-title-dark" : "card-title-text"} >
                    Target
                  </h1>
                  <h1 className={(isHighLighted) ? "card-count-dark" : "card-count"}>
                    $ 750 000
                  </h1>
                  <h1 className={(isHighLighted) ? "card-count-text-dark" : "card-count-text"}>
                    5 000 000 Locamo
                  </h1>
                  <div
                    style={{
                      padding: "45px  0",
                      display: "flex",
                      alignItems: "baseline",
                      justifyContent: "center",
                    }}
                  >
                    <h1 className={(isHighLighted) ? "card-price-dark" : "card-price"}>
                      Locamo Price
                    </h1>
                    <Button className="card-btn">
                      $ 0.012
                    </Button>
                  </div>
                </Card.Text>
                <h1 className={(isHighLighted) ? "card-fixed-dark" : "card-fixed"}>
                  Fixed personal allocation
                </h1>
                <div className="tab-token">
                  <Tabs
                    fill justify
                    style={isHighLighted ? style3 : style1}
                    defaultActiveKey="home"
                    id="uncontrolled-tab-example"
                    className="mb-3 style3"
                  >
                    <Tab
                      eventKey="home"
                      title="$500"
                      style={{ textAlign: "center" }}
                      style={style2}
                    >
                      <h1
                        style={{
                          color: "#ef3340",
                          fontSize: 17,
                          fontWeight: "bold",
                          marginTop: 30,
                        }}
                      >
                        Lockup
                      </h1>
                      <h1 className={(isHighLighted) ? "tab-text-dark" : "tab-text"}>
                        15% are initially unlocked, the remaining 85% has a
                        6-month lockup followed by a 6-month linear release
                      </h1>
                    </Tab>
                    <Tab
                      eventKey="profile"
                      disabled={true}
                      title="3333 LLL"
                      style={{ textAlign: "center" }}
                      style={style2}
                      className={isHighLighted ? "tab-dark-text" : "tab-text-color"}
                    >
                      <h1
                        style={{
                          color: "#ef3340",
                          fontSize: 17,
                          fontWeight: "bold",
                          marginTop: 50,
                        }}
                      >
                        Lockup
                      </h1>
                      <h1 className="tab-text">
                        15% are initially unlocked, the remaining 85% has a
                        6-month lockup followed by a 6-month linear release
                      </h1>
                    </Tab>
                  </Tabs>
                </div>
              </Card.Body>
            </Card>
          </Col>
          <Col
            xs={12}
            md={12}
            lg={4}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
            className="token-sale-stage"
          >
            <Card className="card-width" onMouseEnter={setHighLighted.bind(this, false)} onMouseLeave={setHighLighted.bind(this, false)}>
              <Card.Body>
                <Card.Title className="cart-title">
                  IDO
                </Card.Title>
                <div style={{ borderTop: "1px solid #e6e6e6" }}></div>
                <Card.Text style={{ marginTop: 10, textAlign: "center" }}>
                  <h1 className="card-title-text">
                    Target
                  </h1>
                  <h1 className="card-count">
                    $ 500 000
                  </h1>
                  <h1 className="card-count-text">
                    2 500 000 Locamo
                  </h1>
                  <div
                    style={{
                      padding: "45px  0",
                      display: "flex",
                      alignItems: "baseline",
                      justifyContent: "center",
                    }}
                  >
                    <h1 className="card-price">
                      Locamo Price
                    </h1>
                    <Button className="card-btn" >
                      $ 0.015
                    </Button>
                  </div>
                </Card.Text>
                <h1 className="card-fixed" >
                  Fixed personal allocation
                </h1>
                <div className="tab-token">
                  <Tabs
                    style={{
                      backgroundColor: "rgba(65,73,107,0.10196078431372549 )",
                      borderRadius: 30,
                      margin: 0
                    }}
                    fill justify
                    style={style1}
                    defaultActiveKey="home"
                    id="uncontrolled-tab-example"
                    className="mb-3 uncontrolled"
                  >
                    <Tab
                      eventKey="home"
                      title="$500"
                      style={{ textAlign: "center" }}
                      style={style2}
                    >
                      <h1
                        style={{
                          color: "#ef3340",
                          fontSize: 17,
                          fontWeight: "bold",
                          marginTop: 30,
                        }}
                      >
                        Lockup
                      </h1>
                      <h1 className="tab-text">
                        25% are initially unlocked, the remaining 75% has a
                        3-month linear release
                      </h1>
                    </Tab>
                    <Tab
                      eventKey="profile"
                      disabled={true}
                      title="5000 LLL"
                      style={{ textAlign: "center" }}
                      style={style2}
                    >
                      <h1
                        style={{
                          color: "#ef3340",
                          fontSize: 17,
                          fontWeight: "bold",
                          marginTop: 50,
                        }}
                      >
                        Lockup
                      </h1>
                      <h1 className="tab-text">
                        12-month lockup followed by a 12-month linear release
                      </h1>
                    </Tab>
                  </Tabs>
                </div>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={12}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Button className="red-card-btn">
              Join
            </Button>
          </Col>
        </Row>
      </Container>
      <Container className="token-disc">
        <h1 className="header-title" style={{ textAlign: 'center', marginTop: 30, fontWeight: 'bold', padding: '15px 40px', }}>Token Distribution</h1>
        <Row style={{ backgroundColor: '#163d47', padding: 28, borderRadius: 10 }} className="text-align">
          <Col xs={4} className="image-hover">
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip}
            >
              <div style={{ width: '100%', flexDirection: 'column' }} className="text-align">
                <h1 className="text-line">52%</h1>
                <div style={{ backgroundColor: '#ef3340', width: '100%', height: 10, borderRadius: 15 }} />
              </div>
            </OverlayTrigger>
          </Col>
          <Col xs={2} className="image-hover">
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip1}
            >
              <div style={{ width: '100%', flexDirection: 'column' }} className="text-align">

                <h1 className="text-line">20%</h1>
                <div style={{ backgroundColor: '#ef3340', width: '100%', height: 10, borderRadius: 15 }} />
              </div>
            </OverlayTrigger>
          </Col>
          <Col xs={2} className="image-hover">
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip2}
            >
              <div style={{ width: '100%', flexDirection: 'column' }} className="text-align">

                <h1 className="text-line">18%</h1>
                <div style={{ backgroundColor: '#ef3340', width: '100%', height: 10, borderRadius: 15 }} />
              </div>
            </OverlayTrigger>
          </Col>
          <Col xs={1} className="image-hover">
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip3}
            >
              <div style={{ width: '100%', flexDirection: 'column' }} className="text-align">

                <h1 className="text-line">4%</h1>
                <div style={{ backgroundColor: '#ef3340', width: '100%', height: 10, borderRadius: 15 }} />
              </div>
            </OverlayTrigger>
          </Col>
          <Col xs={1} className="image-hover">
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip4}
            >
              <div style={{ width: '100%', flexDirection: 'column' }} className="text-align">

                <h1 className="text-line">2%</h1>
                <div style={{ backgroundColor: '#ef3340', width: '100%', height: 10, borderRadius: 15 }} />
              </div>
            </OverlayTrigger>
          </Col>
          <Col xs={1} className="image-hover">
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip5}
            >
              <div style={{ width: '100%', flexDirection: 'column' }} className="text-align">

                <h1 className="text-line">2%</h1>
                <div style={{ backgroundColor: '#ef3340', width: '100%', height: 10, borderRadius: 15 }} />
              </div>
            </OverlayTrigger>
          </Col>
          <Col xs={1} className="image-hover">
            <OverlayTrigger
              placement="top"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip6}
            >
              <div style={{ width: '100%', flexDirection: 'column' }} className="text-align">

                <h1 className="text-line">2%</h1>
                <div style={{ backgroundColor: '#ef3340', width: '100%', height: 10, borderRadius: 15 }} />
              </div>
            </OverlayTrigger>
          </Col>
        </Row>
      </Container>
      <Container id='team' style={{ padding: "70px 0" }}>
        <h1 style={{ textAlign: "center", marginTop: 50, marginBottom: 30 }} className="header-title">Team</h1>
        <Row>
          {data?.map((item, index) => (
            <Col
              key={index}
              xs={12}
              sm={6}
              md={6}
              lg={4}
              className="team-card"
            >
              <div className="d-flex justify-content-start align-items-center bg">
                <Image style={{ width: 95, height: 95 }} src={item.img} />
                <div style={{ marginLeft: 20 }} className='text-token-disc'>
                  <h1 className="text" style={{ fontSize: 20, color: "#424242", textTransform: 'capitalize' }}>{item.name}</h1>
                  <h1 style={{ fontSize: 15, color: "#8f8f8f", textTransform: 'capitalize' }}>{item.des}</h1>
                </div>
              </div>
            </Col>
          ))}
        </Row>
      </Container>
      <Container className="timeline">
        <h1 className="header-title">
          Timeline
        </h1>
        <Row>
          <Col className="dot" xs={12} sm={12} md={3}>
            <Row>
              <Col xs={12} sm={3}>
                <h1 className="bollete"></h1>
              </Col>
              <Col xs={12} sm={9} className="dot-text">
                <h1 className="time-text">
                  September 2021
                </h1>
              </Col>
            </Row>
          </Col>
          <Col sm={12} md={9} style={{ paddingLeft: 37 }}>
            <h1 style={{ color: "#163d47", textAlign: "left", fontSize: 20, fontWeight: 'bold' }} className='montserrat'>
              Locamo Alpha Launch
            </h1>
          </Col>
          <Col className="dot" sm={12} md={3}>
            <Row>
              <Col xs={12} sm={3}>
                <h1
                  className="bollete"
                  style={{
                    backgroundColor: "#ef3340",
                    width: 22,
                    borderRadius: "50%",
                    height: 22,
                  }}
                ></h1>
              </Col>
              <Col xs={12} sm={9} className="dot-text">
                <h1 className="time-text">
                  November 2021
                </h1>
              </Col>
            </Row>
          </Col>
          <Col sm={12} md={9} style={{ paddingLeft: 37 }}>
            <h1 style={{ color: "#163d47", textAlign: "left", fontSize: 20, fontWeight: 'bold', textTransform: "none" }}>
              Beta Launch
            </h1>
          </Col>
          <Col sm={12} md={3}>
            <Row>
              <Col xs={12} sm={3}>
                <h1
                  className="bollete1"
                  style={{
                    backgroundColor: "#ef3340",
                    width: 22,
                    borderRadius: "50%",
                    height: 22,
                  }}
                ></h1>
              </Col>
              <Col xs={12} sm={9} className="dot-text">
                <h1 className="time-text">January</h1>
              </Col>
            </Row>
          </Col>
          <Col sm={12} md={9} style={{ paddingLeft: 37 }}>
            <h1 style={{ color: "#163d47", textAlign: "left", fontSize: 20, fontWeight: 'bold', textTransform: "none" }} >
              Pre-release Locamo Token Purchase
            </h1>
            <ul className="dotted">
              <li>
                <h1 style={{ color: "#424242", fontSize: 15, marginLeft: 10 }}>
                  A single global registry of transactions is provided.
                </h1>
              </li>
              <li>
                <h1 style={{ color: "#424242", fontSize: 15, marginLeft: 10 }}>
                  Every every account, every transaction is made eternally
                  reliable and immutable.
                </h1>
              </li>
              <li>
                <h1 style={{ color: "#424242", fontSize: 15, marginLeft: 10 }}>
                  Personal keys protection.
                </h1>
              </li>
              <li>
                <h1 style={{ color: "#424242", fontSize: 15, marginLeft: 10 }}>
                  Disclosure of the Whitepaper.
                </h1>
              </li>
            </ul>
          </Col>
          <Col xs={12} style={{ marginLeft: 15, display: "flex", marginTop: 28, marginBottom: 60, alignItems: 'baseline' }}>
            <h1 style={{ color: "#a3a3a3", fontWeight: "bold", fontSize: 18 }} className="line">
              UPCOMING
            </h1>
            <div className="up-comming"></div>
          </Col>
          <Col className="dot" sm={12} md={3}>
            <Row>
              <Col xs={12} sm={3}>
                <h1
                  className="bollete3"
                  style={{
                    backgroundColor: "#ef3340",
                    width: 22,
                    borderRadius: "50%",
                    height: 22,
                  }}
                ></h1>
              </Col>
              <Col xs={12} sm={9} className="dot-text">
                <h1 className="time-text">March</h1>
              </Col>
            </Row>
          </Col>
          <Col sm={12} md={9} style={{ paddingLeft: 37 }}>
            <h1 style={{ color: "#163d47", textAlign: "left", fontSize: 20, fontWeight: 'bold', textTransform: "none" }} >
              Blockchain Platform Testing
            </h1>
          </Col>
          <Col className="dot" sm={12} md={3}>
            <Row>
              <Col xs={12} sm={3}>
                <h1
                  style={{
                    backgroundColor: "#ef3340",
                    width: 22,
                    borderRadius: "50%",
                    height: 22,
                  }}
                ></h1>
              </Col>
              <Col xs={12} sm={9} className="dot-text">
                <h1 className="time-text">June</h1>
              </Col>
            </Row>
          </Col>
          <Col sm={12} md={9} style={{ paddingLeft: 37 }}>
            <h1 style={{ color: "#163d47", textAlign: "left", fontSize: 20, fontWeight: 'bold', textTransform: "none" }} >
              Blockchain Release
            </h1>
          </Col>
        </Row>
      </Container>
      <Container id="contact" style={{ padding: "50px 10px" }}>
        <Row>
          <Col
            xs={12}
            sm={4}
            style={{
              display: "flex",
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <h1 style={{ color: "#424242", fontSize: 18 }}>642316</h1>
            <h1 style={{ color: "#424242", fontSize: 18 }}>
              West Declan, Belarus <br />
              0421 Stehr Estate Apt. 43
            </h1>
            <div style={{ marginTop: 50 }}>
              <h1 style={{ color: "#424242", fontSize: 18 }}>
                <span style={{ marginRight: 5 }}>
                  <Image src="assets/images/phone (1).png" />
                </span>{" "}
                8 800 593-170-2995
              </h1>
              <h1 style={{ color: "#424242", fontSize: 18 }}>
                <span style={{ marginRight: 5 }}>
                  <Image src="assets/images/phone (2).png" />
                </span>{" "}
                mail@locamo.ae
              </h1>
            </div>
          </Col>
          <Col xs={12} sm={8}>
            <h1 className="header-title">
              Contact Us
            </h1>
            <div
              style={{
                boxShadow: "0 0 12px rgb(60 72 88 / 25%)",
                backgroundColor: "#FFF",
                borderRadius: 10,
                padding: 30,
              }}
            >
              <Row>
                <Col xs={12} sm={6}>
                  <Col xs={12} sm={6}></Col>
                  <LocamoFormInput
                    label="Your name*"
                    placeholder="Your name."
                  />
                </Col>
                <Col xs={12} sm={6}>
                  <LocamoFormInput
                    label="Contact email*"
                    placeholder="you@example.com"
                  />
                </Col>
                <Col xs={12} sm={6}>
                  <Col xs={12} sm={6}></Col>
                  <LocamoFormInput
                    label="Company name*"
                    placeholder="Company name"
                  />
                </Col>
                <Col xs={12} sm={6}>
                  <Form style={{ marginBottom: 15 }}>
                    <Form.Label>Country*</Form.Label>
                    <Form.Select aria-label="Country">
                      <option>Country</option>
                      <option value="1">India</option>
                      <option value="2">US</option>
                    </Form.Select>
                  </Form>
                </Col>
                <Col xs={12} sm={12}>
                  <LocamoFormInput
                    label="Your message*"
                    as="textarea"
                    placeholder="Type your message…."
                  />
                </Col>
                <Col xs={12} sm={12}>
                  <h1 style={{ fontSize: 13, color: "#5a7184", fontWeight: 'normal' }}>
                    By submitting this form you agree to our terms and
                    conditions and our Privacy Policy which explains how we may
                    collect, use and disclose your personal information
                    including to third parties.
                  </h1>
                </Col>
                <Col xs={12} sm={12}>
                  <Button
                    className="red-contact-btn"
                    style={{
                      fontWeight: "bold",
                      border: 'none'
                    }}
                  >
                    Contact sales
                  </Button>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </Container>
      <LoacmoFooter />
    </div >
  );
};

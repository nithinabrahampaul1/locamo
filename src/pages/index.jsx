import React from "react";
import { Home } from "./home";

const Pages = () => {
  return (
    <React.Fragment>
      <Home />
    </React.Fragment>
  );
};

export default Pages;

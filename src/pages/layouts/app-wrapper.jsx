import React from "react";
import { CookiesProvider } from "react-cookie";
import { AuthProvider, LoaderProvider } from "../../common/contexts";

export const AppWrapper = ({ children }) => {
  return (
    <React.Fragment>
      <CookiesProvider>
        <LoaderProvider>
          <AuthProvider>{children}</AuthProvider>
        </LoaderProvider>
      </CookiesProvider>
    </React.Fragment>
  );
};

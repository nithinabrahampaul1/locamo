import React from "react";
import { AppRoutes } from "./pages/layouts/app-routes";
import { AppWrapper } from "./pages/layouts/app-wrapper";
import "@fortawesome/fontawesome-free/css/all.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./App.css";
import "./index.css";

const App = () => {
  return (
    <React.Fragment>
      <AppWrapper>
        <AppRoutes />
      </AppWrapper>
    </React.Fragment>
  );
};

export default App;
